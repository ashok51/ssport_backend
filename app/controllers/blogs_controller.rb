class BlogsController < ApplicationController
    include Devise::Controllers::Helpers
    before_action :authenticate_user!, :except => [:index]
    
    def index
      render json: Blog.all
    end

    def create
        blog=Blog.create(blog_params.merge(user_id: current_user.id))
        render json: blog
    end

    def show
        render json: Blog.find_by(id: params[:id])
    end

    def update
        @blog=Blog.find_by(blog_params[:id])
        render json: @blog.update(blog_params)        
    end

    def destroy
        @blog=Blog.find(params[:id])
        render json: @blog.destroy
    end
    

    private

    def blog_params                #strong parameters
        params.require(:blog).permit(:title, :body)
    end
end
